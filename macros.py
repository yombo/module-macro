"""
Allows calling several device commands from a virtual command.
This allows the user to create a "macro" virtual device to
control several devices quickly.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: Copyright 2012 by Yombo.
:license: See LICENSE file for details.
"""
from twisted.internet import reactor

from yombo.core.module import YomboModule
from yombo.core.helpers import getTimes
from yombo.core.log import getLogger

logger = getLogger("module.macros")

class Macros(YomboModule):
    """
    Macros module.
    """
    def init(self):
        self._ModDescription = "Macros module to control a group of devices."
        self._ModAuthor = "Mitch Schwenk @ Yombo"
        self._ModUrl = "http://www.yombo.net"
        
    def load(self):
        pass

        
    def start(self):
        pass
    
    def stop(self):
        pass
    
    def unload(self):
        pass

    def message(self, message):
        """
        Incomming Yombo Messages from the gateway or remote sources will
        be sent here.
        """

        if message.msgDestination != self._FullName.lower():
            return  #we don't care about other people for now.

        if message.msgType == 'cmd' and message.msgStatus == 'new':
            device = message.payload['deviceobj']

        
        
            
            
