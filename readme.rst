Yombo Macros Module
====================

This module provides an ability to execute several commands by calling a
single command. This module defines a new "virtual device" that is, it's a
device that is not real but is used to setup the macro.

An example: The user creates a new device called "All Lights Off" with a
description of "Turn off all the lights."  After the user creates this
virtual device, several devices (either real or other virutal devices) can
be attached to this "All Lights Off" device.  In this example, various light
devices would be set with the command of "Off".
